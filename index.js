
// generar array de 1 000 000 de numeros
function generateRandomArray(length) {
  const randomArray = [];
  for (let i = 0; i < length; i++) {
    const randomNumber = Math.floor(Math.random() * 100) + 1;
    randomArray.push(randomNumber);
  }
  return randomArray;
}

function findMatchingPairOptimized(numbers, sum) {
  
  const seenNumbers = new Set();
  for (let i = 0; i < numbers.length; i++) {
    
    //calculando el complemento del número actual
    const complement = sum - numbers[i];
            
    if (seenNumbers.has(complement)) {
      // Si se encuentra el complemento en el conjunto, devuelve la pareja   
      return [complement, numbers[i]];
    }

    // Agrega el número actual al conjunto    
    seenNumbers.add(numbers[i]);
  }

  // Si no se encuentra ninguna pareja, devuelve null
  return null;
}

const sequentialNumbers = generateRandomArray(1000000);


console.log(findMatchingPairOptimized([1,3,3,7], 9));  // Debería imprimir null
console.log(findMatchingPairOptimized([2,3,6,7], 9));  // Debería imprimir [3, 6]
console.log(findMatchingPairOptimized([1, 3, 3, 7], 9));  // Debería imprimir null
console.log(findMatchingPairOptimized(sequentialNumbers, 1000000));// Debería imprimir null
