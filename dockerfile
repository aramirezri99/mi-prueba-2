# Usa una imagen base con Node.js
FROM node:14

# Establece el directorio de trabajo en /app
WORKDIR /app

# Copia el script de JavaScript al directorio de trabajo
COPY index.js .

# Instala las dependencias si es necesario (por ejemplo, si hay un archivo package.json)
RUN npm install

# Ejecuta el linter
RUN lint

# Comando para ejecutar las pruebas unitarias
RUN npm test

# Comando para ejecutar el script
CMD ["node", "index.js"]
