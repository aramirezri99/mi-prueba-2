# para compilar necesitas tener docker instalado
# ejecutar estos comandoss

# crear la imagen
docker buildx build -t node-image .

# ejecuta el contenedor basado en la imagen
docker run node-image

O tambien puedes ejecutar con la extension LiveServe de Visual Studio Code
dando click en Open with Live Server en el archivo index.html

Luego de ejecutado puedes presionar CONTROL + SHIFT + I y te llevara
al inpector del navegador o puedes hacerlo manualmente con anticlick y click
en inpeccionar

y por ultimo te vas a la seccion Console y mostrar la lineas del archivo index.js ejecutandose
